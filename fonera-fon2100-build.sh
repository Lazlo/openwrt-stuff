#!/bin/sh

#set -x
deps="gcc binutils bzip2 flex python perl make find grep diff unzip gawk"
deps="$deps getopt subversion libz-dev libc-dev libncurses5-dev"
for dep in $deps; do
	dpkg-query -W $dep
done
if [ ! -e openwrt ]; then
	git clone https://git.openwrt.org/openwrt.git
fi
cd openwrt
./scripts/feeds update -a
cp ../wobble-dotconfig .config
make -j8
cd -
